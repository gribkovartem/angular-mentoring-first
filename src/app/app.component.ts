import { Component } from '@angular/core';
import Breadcrumb from './shared/breadcrumbs/breadcrumb';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})
export class AppComponent {
  public title = 'app';
  public breadcrumbs: Array<Breadcrumb> = [
    {text: 'Courses', url: '/'}
  ];
}
