export default interface Course {
    id: string;
    title: string;
    creationDate: Date;
    duration: number;
    description: string;
    imageLink: string;
}
