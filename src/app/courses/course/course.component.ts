import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import Course from './course';

@Component({
  selector: 'app-course',
  templateUrl: './course.component.html',
  styleUrls: ['./course.component.scss']
})
export class CourseComponent implements OnInit {
  @Input() item: Course;
  @Output() afterDelete = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  delete() {
    console.log('delete element');
    this.afterDelete.emit();
  }
}
