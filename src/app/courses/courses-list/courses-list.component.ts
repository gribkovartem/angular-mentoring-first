import { Component, OnInit } from '@angular/core';
import Course from '../course/course';

@Component({
  selector: 'app-courses-list',
  templateUrl: './courses-list.component.html',
  styleUrls: ['./courses-list.component.scss']
})
export class CoursesListComponent implements OnInit {
  public courses: Array<Course>;

  constructor() { }

  ngOnInit() {
    this.courses = [
      {
        id: '1',
        title: 'Video Course 1',
        creationDate: new Date('05.29.2018'),
        duration: 1.28,
        // tslint:disable-next-line:max-line-length
        description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
        imageLink: 'https://www.trycatchclasses.com/wp-content/uploads/2017/03/AngularJS-2-1.jpg'
      },
      {
        id: '2',
        title: 'Video Course 2',
        creationDate: new Date('06.10.2018'),
        duration: 0.27,
        // tslint:disable-next-line:max-line-length
        description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
        imageLink: 'https://www.trycatchclasses.com/wp-content/uploads/2017/03/AngularJS-2-1.jpg'
      },
      {
        id: '3',
        title: 'Video Course 2',
        creationDate: new Date('06.10.2018'),
        duration: 0.27,
        // tslint:disable-next-line:max-line-length
        description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
        imageLink: 'https://www.trycatchclasses.com/wp-content/uploads/2017/03/AngularJS-2-1.jpg'
      },
      {
        id: '4',
        title: 'Video Course 2',
        creationDate: new Date('06.10.2018'),
        duration: 0.27,
        // tslint:disable-next-line:max-line-length
        description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
        imageLink: 'https://www.trycatchclasses.com/wp-content/uploads/2017/03/AngularJS-2-1.jpg'
      },
      {
        id: '5',
        title: 'Video Course 2',
        creationDate: new Date('06.10.2018'),
        duration: 0.27,
        // tslint:disable-next-line:max-line-length
        description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
        imageLink: 'https://www.trycatchclasses.com/wp-content/uploads/2017/03/AngularJS-2-1.jpg'
      },
      {
        id: '6',
        title: 'Video Course 2',
        creationDate: new Date('06.10.2018'),
        duration: 0.27,
        // tslint:disable-next-line:max-line-length
        description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
        imageLink: 'https://www.trycatchclasses.com/wp-content/uploads/2017/03/AngularJS-2-1.jpg'
      }
    ];
  }

  showId(id: string) {
    console.log(id);
  }

  loadMore() {
    console.log('load more');
  }

}
