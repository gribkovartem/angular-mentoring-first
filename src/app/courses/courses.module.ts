import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatIconModule } from '@angular/material/icon';
import { CoreModule } from '../core/core.module';
import { SharedModule } from '../shared/shared.module';
import { CourseComponent } from './course/course.component';
import { CoursesFormComponent } from './courses-form/courses-form.component';
import { CoursesListComponent } from './courses-list/courses-list.component';
import { CoursesComponent } from './courses.component';

@NgModule({
  imports: [
    CommonModule,
    CoreModule,
    MatCardModule,
    MatButtonModule,
    MatIconModule,
    SharedModule
  ],
  declarations: [
    CoursesComponent,
    CourseComponent,
    CoursesListComponent,
    CoursesFormComponent
  ],
  exports: [
    CoursesComponent
  ]
})
export class CoursesModule { }
