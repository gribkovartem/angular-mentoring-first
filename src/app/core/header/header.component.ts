import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import User from '../user';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  public user: User = {
    id: '1',
    firstName: 'User',
    lastName: 'Login',
    login: 'User Login'
  };

  constructor() { }

  ngOnInit() {
  }

}
