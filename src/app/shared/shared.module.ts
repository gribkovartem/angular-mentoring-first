import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { BreadcrumbsComponent } from './breadcrumbs/breadcrumbs.component';
import { SearchComponent } from './search/search.component';

@NgModule({
  imports: [
    CommonModule,
    MatInputModule,
    FormsModule,
    MatButtonModule
  ],
  declarations: [
    BreadcrumbsComponent,
    SearchComponent
  ],
  exports: [
    BreadcrumbsComponent,
    SearchComponent
  ]
})
export class SharedModule { }
