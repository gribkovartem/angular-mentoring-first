export default interface Breadcrumb {
    text: string;
    url: string;
}
